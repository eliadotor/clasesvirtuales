Elia Dotor Puente
-----------------

Ejercicios y prácticas clases virtuales
=======================================

Este repositorio contendrá todos aquellos ejercicios y prácticas realizados durante las sesiones de las clases virtuales.

###Ejercicio 1 - Peliculas ECV0510

En el ejercicio 1 del diagrama de clases **Películas** (Solución 1), he creado la **clase Pelicula**, la **clase Persona**, la *clase Trailer* y el **enumerado Genero**.

En cada clase he creado un *constructor* y los metodos *setters and getters* para poder acceder a los atributos que están declarados como privados.

En el *enumerado Genero* he incluido todo los valores que puede tomar.

En la *clase pelicula* he creado un nuevo trailer dentro del constructor ya que la *clase trailer* tiene una relación de **composición** con esta clase.
Sin embargo, las colecciones que he creado de *actores*, *productores*, *directores* y *guionistas*, todas ellas un **ArrayList** de tipo *Persona*, no las he incluido en el contructor ya que solo se muestran en el diagráma como relaciones de **asociación** y no de *composición* o *agregación*.

(Esto último no sé si es correcto por lo que espero a comprobarlo con tu solución o si no con la futura explicación para poderlo hacer adecuadamente en la práctica.

En la *clase Persona*, he dejado del mismo modo la coleccion peliculas, fuera del constructor y la he implementado como un **TreeSet**. 

(También me gustaría si pudieses hacernos una breve aclaración entre la utilización de las mismas).

Por último, en la *clase Trailer* la colección que he creado también la he implementado como **TreeSet**.

______________________________________________________________________________________________

###Ejercicio 2 - Peliculas ECV0520

En el ejercicio 2 del diagrama de clases **Películas** (Solución 2), he creado la **clase Pelicula**, la **clase Persona**, la **clase Trailer**, la **clase Actor**, la **clase Director**, la **clase Editor**, la **clase Guionista**, la **clase Productor** y el **enumerado Genero**.

En cada clase he creado un *constructor* y los metodos *setters and getters* para poder acceder a los atributos que están declarados como privados.

Hay que tener en cuenta que en las clases *Actor, Director, Editor, Guionista y Productor* el constructor y los métodos getters y setters los heredan de la *superclase Persona*, ya que todas ellas tienen una relación de **herencia**.

En el *enumerado Genero* he incluido todo los valores que puede tomar.

En la *clase pelicula* he creado un nuevo trailer dentro del constructor ya que la *clase trailer* tiene una relación de **composición** con esta clase.
Sin embargo, las colecciones que he creado de *actores*, *editores*, *productores*, *directores* y *guionistas*, todas ellas se implementan con un **ArrayList** pero cada una con el tipo de la clase con la que tienen la relación.
Al igual que te expliqué en el ejercicio 1, no las he incluido en el contructor ya que solo se muestran en el diagráma como relaciones de **asociación** y no de *composición* o *agregación*.


En esta ocasión la *clase Persona*, solo tiene relación de **herencia**, por lo que no creamos en ella ninguna colección, ya que se han creado en las subclases.


Por último, en la *clase Trailer* la colección que he creado la he implementado como **TreeSet**.

_________________________________________________________________________________________________________________

###Práctica 1 - Rugby 6 Naciones PCV05-10

En la práctica del diagrama de clases **Rugby 6 Naciones**, he creado la **clase Torneo**, la **clase Jornada**, la **clase Partido**, la **clase Estadio**, la **clase Equipo**, la **clase Entrenador**, la **clase Arbitro**, la **clase Jugador**, la **clase Persona** y el **enumerado Posicion** y el **enumerado Pais**.

En cada clase he creado un *constructor* y los metodos *setters and getters* para poder acceder a los atributos que están declarados como privados.

Hay que tener en cuenta que las clases *Arbitro, Entrenador y Jugador* el constructor y los métodos getters y setters los heredan de la *superclase Persona*, ya que todas ellas tienen una relación de **herencia**.

A su vez, la clase *Jugador* va a tener un objeto de su misma clase, que será el capitán. 


En la *clase Torneo* he creado una nueva Jornada dentro del constructor ya que la *clase Jornada* tiene una relación de **composición** con esta. Y a su vez, dentro de la *clase Jornada*, en su constructor he creado un nuevo Partido, ya que la *clase Partido* tiene también una relación de **composición** con ella.


En todas las clase que tienen una relación de **asociación** con multiplicidad 0..1 o 1, he incluido un objeto del tipo de la clase con la que tienen la relación.

Sin embargo, las colecciones que he creado en el resto de clases, todas ellas se implementan con un **ArrayList** pero cada una con el tipo de la clase con la que tienen la relación. De estas colecciones también he creado getters and setters, para poder acceder a ellas y además les he añadido un *método add* y un *método remove* para poder añadir y eliminar elementos de la colección.


En ambos enumerados he incluido todo los valores que puede tomar cada uno.


