package ecv0510;

import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;

public class Persona {
	private String nombre;
	private String apellidos;
	private Date fechaNacimiento;
	private String nacionalidad;
	public Collection peliculas = new TreeSet();
	

	public Persona(String nombre, String apellidos, Date fechaNacimiento, String nacionalidad) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaNacimiento = fechaNacimiento;
		this.nacionalidad = nacionalidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
}
