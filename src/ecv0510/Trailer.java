package ecv0510;

import java.util.Collection;
import java.util.TreeSet;
import java.net.URI;
import java.sql.Time;

public class Trailer {
	private URI url;
	private Time duracion;
	public Collection editor = new TreeSet();
	
	public Trailer(URI url, Time duracion) {
		this.url = url;
		this.duracion = duracion;
	}

	public URI getUrl() {
		return url;
	}
	
	public void setUrl(URI url) {
		this.url = url;
	}
	
	public Time getDuracion() {
		return duracion;
	}
	
	public void setDuracion(Time duracion) {
		this.duracion = duracion;
	}
}
