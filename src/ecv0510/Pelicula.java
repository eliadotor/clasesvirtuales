package ecv0510;

import java.util.ArrayList;
import java.util.Collection;
import ecv0510.Trailer;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Time;
import java.time.Year;

public class Pelicula {
	private String titulo;
	private Year agno;
	private String sinopsis;
	private Genero genero;
	private String pais;
	public Trailer trailer;
	public Collection actorer = new ArrayList<Persona>();
	public Collection directores = new ArrayList<Persona>();
	public Collection productores = new ArrayList<Persona>();
	public Collection guionistas = new ArrayList<Persona>();
	
	
	
	public Pelicula(String titulo, Year agno, String sinopsis, Genero genero, String pais){
		this.titulo = titulo;
		this.agno = agno;
		this.sinopsis = sinopsis;
		this.genero = genero;
		this.pais = pais;
		try {
			trailer = new Trailer(new URI("www.iesgoya.es"), new Time(3));
		}catch(URISyntaxException err) {
			System.out.println("ERROR.");
		}

	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Year getAgno() {
		return agno;
	}

	public void setAgno(Year agno) {
		this.agno = agno;
	}

	public String getSinopsis() {
		return sinopsis;
	}

	public void setSinopsis(String sinopsis) {
		this.sinopsis = sinopsis;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	
	
}
