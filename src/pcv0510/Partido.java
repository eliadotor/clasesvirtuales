package pcv0510;

public class Partido {
	private int puntosLocal;
	private int puntosVisitante;
	private int bonusLocal;
	private int bonusVisitante;
	private Estadio estadio;
	private Equipo local;
	private Equipo visitante;
	private Arbitro arbitro;
	
	

	public Partido(int puntosLocal, int puntosVisitante, int bonusLocal, int bonusVisitante, Estadio estadio,
			Equipo local, Equipo visitante, Arbitro arbitro){
		this.puntosLocal = puntosLocal;
		this.puntosVisitante = puntosVisitante;
		this.bonusLocal = bonusLocal;
		this.bonusVisitante = bonusVisitante;
		this.estadio = estadio;
		this.local = local;
		this.visitante = visitante;
		this.arbitro = arbitro;
	}

	public int getPuntosLocal() {
		return puntosLocal;
	}

	public void setPuntosLocal(int puntosLocal) {
		this.puntosLocal = puntosLocal;
	}

	public int getPuntosVisitante() {
		return puntosVisitante;
	}

	public void setPuntosVisitante(int puntosVisitante) {
		this.puntosVisitante = puntosVisitante;
	}

	public int getBonusLocal() {
		return bonusLocal;
	}

	public void setBonusLocal(int bonusLocal) {
		this.bonusLocal = bonusLocal;
	}

	public int getBonusVisitante() {
		return bonusVisitante;
	}

	public void setBonusVisitante(int bonusVisitante) {
		this.bonusVisitante = bonusVisitante;
	}
	
	public Estadio getEstadio() {
		return estadio;
	}

	public void setEstadio(Estadio estadio) {
		this.estadio = estadio;
	}

	public Equipo getLocal() {
		return local;
	}

	public void setLocal(Equipo local) {
		this.local = local;
	}

	public Arbitro getArbitro() {
		return arbitro;
	}

	public void setArbitro(Arbitro arbitro) {
		this.arbitro = arbitro;
	}

	public void resultado() {
		System.out.println("El resultado del partido es: \nLocal: " + getPuntosVisitante() + getBonusVisitante() + " puntos" 
		+ "\nVisitante: " + getPuntosVisitante() + getBonusVisitante() + " puntos");
	}
	
	



}
