package pcv0510;

import java.util.Date;

public class Jugador extends Persona{
	private Posicion posicion;
	private Equipo equipo;
	private Jugador capitan;
	
	public Jugador(String nombre, Date fechaNacimiento, Posicion posicion, Equipo equipo, Jugador capitan) {
		super(nombre, fechaNacimiento);
		this.posicion = posicion;
		this.equipo = equipo;
		this.capitan = capitan;
	}

	public Posicion getPosicion() {
		return posicion;
	}

	public void setPosicion(Posicion posicion) {
		this.posicion = posicion;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public Jugador getCapitan() {
		return capitan;
	}

	public void setCapitan(Jugador capitan) {
		this.capitan = capitan;
	}

	
	
	
	
}
