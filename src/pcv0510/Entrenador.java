package pcv0510;

import java.util.Date;

public class Entrenador extends Persona{
	private Equipo equipo;

	public Entrenador(String nombre, Date fechaNacimiento, Equipo equipo) {
		super(nombre, fechaNacimiento);
		this.equipo = equipo;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	
	

}
