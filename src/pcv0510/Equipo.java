package pcv0510;

import java.util.ArrayList;
import java.util.Collection;

public class Equipo {
	private Pais nombre;
	private Entrenador entrenador;
	private Estadio estadio;
	private Collection jugadores = new ArrayList<Jugador>();
	private Collection partidos = new ArrayList<Partido>();
	
	
	
	public Equipo(Pais nombre, Entrenador entrenador, Estadio estadio, Collection jugadores, Collection partidos){
		this.nombre = nombre;
		this.entrenador = entrenador;
		this.estadio = estadio;
		this.jugadores = jugadores;
		this.partidos = partidos;
	}

	public Equipo(Pais nombre) {
		this.nombre = nombre;
	}

	public Pais getNombre() {
		return nombre;
	}

	public void setNombre(Pais nombre) {
		this.nombre = nombre;
	}

	public Entrenador getEntrenador() {
		return entrenador;
	}

	public void setEntrenador(Entrenador entrenador) {
		this.entrenador = entrenador;
	}

	public Estadio getEstadio() {
		return estadio;
	}

	public void setEstadio(Estadio estadio) {
		this.estadio = estadio;
	}

	public Collection getJugadores() {
		return jugadores;
	}

	public void setJugadores(Collection jugadores) {
		this.jugadores = jugadores;
	}
	
	public boolean add(Jugador jugador) {
		//Devolvera true si la colección cambia después de llamar al método, es decir, si añade un jugador. 
		return true;
	}
	
	public boolean remove(Jugador jugador) {
		//Devuelve true si la colección cambia después de llamar al método, es decir, elimina un jugador.
		return true;
	}

	public Collection getPartidos() {
		return partidos;
	}

	public void setPartidos(Collection partidos) {
		this.partidos = partidos;
	}
	
	public boolean add(Partido partido) {
		//Devolvera true si la colección cambia después de llamar al método, es decir, si añade un partido. 
		return true;
	}
	
	public boolean remove(Partido partido) {
		//Devuelve true si la colección cambia después de llamar al método, es decir, si elimina un partido.
		return true;
	}
	
	
	
}
