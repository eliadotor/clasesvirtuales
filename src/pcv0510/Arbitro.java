package pcv0510;

import java.util.Date;

public class Arbitro extends Persona{
	private Partido partido;

	public Arbitro(String nombre, Date fechaNacimiento, Partido partido) {
		super(nombre, fechaNacimiento);
		this.partido = partido;
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}
	
	

}
