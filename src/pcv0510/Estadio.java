package pcv0510;

import java.util.ArrayList;
import java.util.Collection;

public class Estadio {
	private String nombre;
	private int capacidad;
	private String ciudad;
	private Equipo equipo;
	private Collection partidos = new ArrayList<Partido>();
	
	public Estadio(String nombre, int capacidad, String ciudad, Equipo equipo, Collection partidos) {
		this.nombre = nombre;
		this.capacidad = capacidad;
		this.ciudad = ciudad;
		this.equipo = equipo;
		this.partidos = partidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public Collection getPartidos() {
		return partidos;
	}

	public void setPartidos(Collection partidos) {
		this.partidos = partidos;
	}
	
	public boolean add(Partido partido) {
		//Devolvera true si la colección cambia después de llamar al método, es decir, si añade un partido. 
		return true;
	}
	
	public boolean remove(Partido partido) {
		//Devuelve true si la colección cambia después de llamar al método, es decir, si elimina un partido.
		return true;
	}
	

	
}
