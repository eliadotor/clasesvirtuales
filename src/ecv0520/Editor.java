package ecv0520;

import java.util.ArrayList;
import java.util.Collection;

public class Editor extends Persona{
	private Collection trailers = new ArrayList<Trailer>();


	public Editor(String nombre, String apellidos, Date fechaNacimiento, String nacionalidad) {
		super(nombre, apellidos, fechaNacimiento, nacionalidad);
	}


	public Collection getTrailers() {
		return trailers;
	}


	public void setTrailers(Collection trailers) {
		this.trailers = trailers;
	}
	
	

}
