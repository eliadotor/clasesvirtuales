package ecv0520;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.net.URI;

public class Trailer {
	private URI url;
	private Time duracion;
    private Pelicula pelicula;
    private Collection editores = new ArrayList<Editor>();
	
	public Trailer(URI url, Time duracion) {
		this.url = url;
		this.duracion = duracion;
	}

	public URI getUrl() {
		return url;
	}
	
	public void setUrl(URI url) {
		this.url = url;
	}
	
	public Time getDuracion() {
		return duracion;
	}
	
	public void setDuracion(Time duracion) {
		this.duracion = duracion;
	}
}
