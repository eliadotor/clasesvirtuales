package ecv0520;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Time;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;

public class Pelicula {
	private String titulo;
	private Year agno;
	private Genero genero;
	private String pais;
	public Trailer trailer;
	public Collection actores = new ArrayList<Actor>();
	public Collection editores = new ArrayList<Editor>();
	public Collection productores = new ArrayList<Productor>();
	public Collection directores = new ArrayList<Director>();
	public Collection guionista = new ArrayList<Guionista>();

	public Pelicula(String titulo, Year agno, Genero genero, String pais){
		this.titulo = titulo;
		this.agno = agno;
		this.genero = genero;
		this.pais = pais;
		try {
			trailer = new Trailer(new URI("www.iesgoya.es"), new Time(3));
		}catch(URISyntaxException err) {
			System.out.println("ERROR: sintaxis URL.");
		}
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Year getAgno() {
		return agno;
	}

	public void setAgno(Year agno) {
		this.agno = agno;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

}
