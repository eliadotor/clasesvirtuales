package ecv0520;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class Productor extends Persona{
    private Collection peliculas = new ArrayList<Pelicula>();

	public Productor(String nombre, String apellidos, Date fechaNacimiento, String nacionalidad) {
		super(nombre, apellidos, fechaNacimiento, nacionalidad);
	}

	public Collection getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(Collection peliculas) {
		this.peliculas = peliculas;
	}
	
	

}
