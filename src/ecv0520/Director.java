package ecv0520;

import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;

public class Director extends Persona{
	public Collection pelicula = new TreeSet();

	public Director(String nombre, String apellidos, Date fechaNacimiento, String nacionalidad) {
		super(nombre, apellidos, fechaNacimiento, nacionalidad);
	}

	public Collection getPelicula() {
		return pelicula;
	}

	public void setPelicula(Collection pelicula) {
		this.pelicula = pelicula;
	}
	
	

}
